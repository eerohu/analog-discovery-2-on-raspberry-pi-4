# Analog Discovery 2 on Raspberry Pi 4
Docker container running Analog Discovery 2 (with all dependencies) on a Raspberry Pi 4

## Getting started
* [Install Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) on your Raspberry Pi 4
* Hook up the Analog Discovery 2 to your Raspberry Pi 4
* Run this Docker container using command `docker run --privileged -it registry.gitlab.com/docker-embedded/analog-discovery-2-on-raspberry-pi-4`
* Test by running the AD2.py script: `python3 AD2.py`